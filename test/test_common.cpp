#include <fmt/core.h>

#include <fstream>
#include <iostream>
#include <list>
#include <memory>
#include <string>

#include "doctest/doctest.h"
#include "graver/util/log_util.h"

TEST_SUITE("test_common") {
    TEST_CASE("test_common_one") {
        std::shared_ptr<spdlog::logger> log = graver::LogUtil::getLogger("test");
        SPDLOG_LOGGER_INFO(log, fmt::format("test_common_one start"));
        SPDLOG_LOGGER_INFO(log, fmt::format("test_common_one end"));
    }
}