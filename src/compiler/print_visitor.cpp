#include "graver/compiler/print_visitor.h"

#include <iostream>

#include "graver/compiler/ast_node.h"

namespace graver {
void PrintVisitor::visitorProgramNode(AstProgramNode* node) {
    node->node()->accept(this);
    std::cout << std::endl;
}
void PrintVisitor::visitorBinaryNode(AstBinaryNode* node) {
    node->right()->accept(this);
    node->left()->accept(this);
    switch (node->op()) {
        case BinaryOperator::ADD: {
            std::cout << " + ";
            break;
        }
        case BinaryOperator::SUB: {
            std::cout << " - ";
            break;
        }
        case BinaryOperator::MUL: {
            std::cout << " * ";
            break;
        }
        case BinaryOperator::DIV: {
            std::cout << " / ";
            break;
        }
    }
}
void PrintVisitor::visitorConstantNode(AstConstantNode* node) {
    std::cout << " " << node->value() << " ";
}
};  // namespace graver