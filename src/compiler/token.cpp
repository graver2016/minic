#include "graver/compiler/token.h"

#include <fmt/core.h>

#include <memory>
#include <sstream>

namespace graver {

Token::Token(TokenKind kind, std::string text, int value) : m_kind(kind), m_value(value), m_text(std::move(text)) {}
Token::Token(TokenKind kind, std::string text) : Token(kind, std::move(text), 0) {}

TokenKind Token::kind() const {
    return this->m_kind;
}
void Token::kind(TokenKind kind) {
    this->m_kind = kind;
}

int Token::value() const {
    return this->m_value;
}
void Token::value(int value) {
    this->m_value = value;
}

std::string Token::text() const {
    return this->m_text;
}
void Token::text(std::string text) {
    this->m_text = std::move(text);
}

std::string Token::toString(const Token& token) {
    const char*       types[] = {"Eof", "BAD", "ADD", "SUB", "MUL", "DIV", "NUM"};
    std::stringstream ss;
    ss << fmt::format("{:<5}", types[token.kind()]);
    ss << ":";
    ss << fmt::format("{:<5}", token.text());
    if (TokenKind::NUM == token.kind()) {
        ss << ":";
        ss << token.value();
    }

    return ss.str();
}

};  // namespace graver