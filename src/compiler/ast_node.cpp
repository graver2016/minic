#include "graver/compiler/ast_node.h"

namespace graver {

void AstProgramNode::accept(AstVisitor *visitor) {
    visitor->visitorProgramNode(this);
}
std::shared_ptr<AstNode> AstProgramNode::node() const {
    return this->m_node;
}
void AstProgramNode::node(std::shared_ptr<AstNode> node) {
    this->m_node = std::move(node);
}

void AstBinaryNode::accept(AstVisitor *visitor) {
    visitor->visitorBinaryNode(this);
}
BinaryOperator AstBinaryNode::op() const {
    return this->m_op;
}
void AstBinaryNode::op(BinaryOperator op) {
    this->m_op = op;
}
std::shared_ptr<AstNode> AstBinaryNode::left() const {
    return this->m_left;
}
void AstBinaryNode::left(std::shared_ptr<AstNode> node) {
    this->m_left = std::move(node);
}
std::shared_ptr<AstNode> AstBinaryNode::right() const {
    return this->m_right;
}
void AstBinaryNode::right(std::shared_ptr<AstNode> node) {
    this->m_right = std::move(node);
}

void AstConstantNode::accept(AstVisitor *visitor) {
    visitor->visitorConstantNode(this);
}
int AstConstantNode::value() const {
    return this->m_value;
}
void AstConstantNode::value(int value) {
    this->m_value = value;
}

};  // namespace graver