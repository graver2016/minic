#include "graver/compiler/code_gen.h"

#include <fmt/core.h>

#include <iostream>

#include "graver/compiler/ast_node.h"

namespace graver {
void CodeGen::visitorProgramNode(AstProgramNode* node) {
    std::cout << fmt::format("\t.text\n");
    std::cout << fmt::format("\t.globl prog\n");
    std::cout << fmt::format("prog:\n");

    std::cout << fmt::format("\tpush %rbp\n");
    std::cout << fmt::format("\tmov %rsp, %rbp\n");
    std::cout << fmt::format("\tsub $32, %rsp\n");

    node->node()->accept(this);

    std::cout << fmt::format("\tmov %rbp, %rsp\n");
    std::cout << fmt::format("\tpop %rbp\n");
    std::cout << fmt::format("\tret\n");
}

void CodeGen::visitorBinaryNode(AstBinaryNode* node) {
    node->right()->accept(this);
    this->push();
    node->left()->accept(this);
    this->pop("%rdi");
    switch (node->op()) {
        case BinaryOperator::ADD: {
            std::cout << fmt::format("\tadd %rdi, %rax\n");
            break;
        }
        case BinaryOperator::SUB: {
            std::cout << fmt::format("\tsub %rdi, %rax\n");
            break;
        }
        case BinaryOperator::MUL: {
            std::cout << fmt::format("\timul %rdi, %rax\n");
            break;
        }
        case BinaryOperator::DIV: {
            std::cout << fmt::format("\tcqo\n");
            std::cout << fmt::format("\tidiv %rdi\n");
            break;
        }
    }
}
void CodeGen::visitorConstantNode(AstConstantNode* node) {
    std::cout << fmt::format("\tmov ${}, %rax\n", node->value());
}

void CodeGen::push() {
    std::cout << fmt::format("\tpush %rax\n");
    this->m_stack_level++;
}
void CodeGen::pop(const char* reg) {
    std::cout << fmt::format("\tpop {}\n", reg);
    this->m_stack_level--;
}
};  // namespace graver