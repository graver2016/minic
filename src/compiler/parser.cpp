#include "graver/compiler/parser.h"

#include <memory>

#include "graver/compiler/ast_node.h"
#include "graver/compiler/lexer.h"
#include "graver/compiler/token.h"

namespace graver {
Parser::Parser(std::shared_ptr<Lexer> lexer) : m_lexer(std::move(lexer)) {}

std::shared_ptr<AstProgramNode> Parser::parse() {
    auto node = std::make_shared<AstProgramNode>();
    node->node(this->parseExpr());
    return node;
}
std::shared_ptr<AstNode> Parser::parseExpr() {
    return this->parseAddExpr();
}
std::shared_ptr<AstNode> Parser::parseAddExpr() {  // NOLINT
    auto left = this->parseMultiExpr();
    while (TokenKind::ADD == this->m_lexer->currToken()->kind() || TokenKind::SUB == this->m_lexer->currToken()->kind()) {
        auto op = BinaryOperator::ADD;
        if (TokenKind::SUB == m_lexer->currToken()->kind()) {
            op = BinaryOperator::SUB;
        }
        this->m_lexer->nextToken();
        auto node = std::make_shared<AstBinaryNode>();
        node->op(op);
        node->left(left);
        node->right(this->parseMultiExpr());
        left = node;
    }
    return left;
}
std::shared_ptr<AstNode> Parser::parseMultiExpr() {  // NOLINT
    auto left = this->parsePrimaryExpr();
    while (TokenKind::MUL == this->m_lexer->currToken()->kind() || TokenKind::DIV == this->m_lexer->currToken()->kind()) {
        auto op = BinaryOperator::MUL;
        if (TokenKind::DIV == m_lexer->currToken()->kind()) {
            op = BinaryOperator::DIV;
        }
        this->m_lexer->nextToken();
        auto node = std::make_shared<AstBinaryNode>();
        node->op(op);
        node->left(left);
        node->right(this->parsePrimaryExpr());
        left = node;
    }
    return left;
}
std::shared_ptr<AstNode> Parser::parsePrimaryExpr() {  // NOLINT
    auto node = std::make_shared<AstConstantNode>();
    node->value(this->m_lexer->currToken()->value());
    this->m_lexer->nextToken();
    return node;
}

}  // namespace graver