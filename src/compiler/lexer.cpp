#include "graver/compiler/lexer.h"

#include <algorithm>
#include <cctype>
#include <memory>
#include <sstream>
#include <vector>

#include "graver/compiler/token.h"

namespace graver {

Lexer::Lexer(const char* source) {
    this->m_source    = source;
    this->m_curr_char = this->m_source[this->m_cursor];
}

std::shared_ptr<Token> Lexer::currToken() {
    return this->m_curr_token;
}

void Lexer::nextToken() {
    while (isspace(this->m_curr_char)) {
        this->nextChar();
    }
    std::vector<char> ops = {'+', '-', '*', '/'};
    switch (this->m_curr_char) {
        case '\0': {
            this->m_curr_token = std::make_shared<Token>(TokenKind::Eof, "");
            this->nextChar();
            break;
        }
        case '+': {
            this->m_curr_token = std::make_shared<Token>(TokenKind::ADD, "+");
            this->nextChar();
            break;
        }
        case '-': {
            this->m_curr_token = std::make_shared<Token>(TokenKind::SUB, "-");
            this->nextChar();
            break;
        }
        case '*': {
            this->m_curr_token = std::make_shared<Token>(TokenKind::MUL, "*");
            this->nextChar();
            break;
        }
        case '/': {
            this->m_curr_token = std::make_shared<Token>(TokenKind::DIV, "/");
            this->nextChar();
            break;
        }
        default: {
            if (isdigit(this->m_curr_char)) {
                this->parseNumberToken();
            } else {
                std::stringstream ss;
                while ('\0' != this->m_curr_char) {
                    ss << this->m_curr_char;
                    this->nextChar();
                    bool inOps = std::find(ops.begin(), ops.end(), this->m_curr_char) == ops.end();
                    if (isspace(this->m_curr_char) || inOps || isdigit(this->m_curr_char)) {
                        break;
                    }
                }
                this->m_curr_token = std::make_shared<Token>(TokenKind::BAD, ss.str());
            }
        }
    }
}

void Lexer::parseNumberToken() {
    std::stringstream ss;

    bool validNumber   = false;
    bool startWithZero = '0' == this->m_curr_char;
    while (isdigit(this->m_curr_char)) {
        ss << this->m_curr_char;
        validNumber = isdigit(this->m_curr_char);
        this->nextChar();
        if (isspace(this->peek())) {
            break;
        }
    }
    if (validNumber && !startWithZero) {
        int value = 0;
        ss >> value;
        this->m_curr_token = std::make_shared<Token>(TokenKind::NUM, std::to_string(value).c_str(), value);
    } else {
        this->m_curr_token = std::make_shared<Token>(TokenKind::BAD, ss.str());
    }
}

void Lexer::nextChar() {
    this->m_cursor++;
    this->m_curr_char = this->peek();
}

char Lexer::peek(size_t cursor) {
    auto index = this->m_cursor + cursor;
    if (index >= this->m_source.length()) {
        return '\0';
    }
    return this->m_source[index];
}

};  // namespace graver