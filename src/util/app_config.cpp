#include "graver/util/app_config.h"

#include <fmt/core.h>

#include <map>
#include <memory>
#include <mutex>
namespace graver {

AppConfig::AppConfig() {
    // 日志级别
#ifdef CPP_GUIDE_LOG_LEVEL
    // 定义映射关系
    const std::map<int, spdlog::level::level_enum> levelMap{
        {0, spdlog::level::off}, {1, spdlog::level::debug},    {2, spdlog::level::info}, {3, spdlog::level::warn},
        {4, spdlog::level::err}, {5, spdlog::level::critical}, {6, spdlog::level::trace}};

    // 根据宏定义的值获取相应的日志级别
    m_log_level = levelMap.at(CPP_GUIDE_LOG_LEVEL);
#else
    m_log_level = spdlog::level::info;
#endif
}

std::shared_ptr<AppConfig> AppConfig::getInstance() {
    static std::shared_ptr<AppConfig> singleton = nullptr;
    static std::mutex                 singletonMutex;

    if (singleton == nullptr) {
        std::unique_lock<std::mutex> lock(singletonMutex);
        if (singleton == nullptr) {
            singleton = std::shared_ptr<AppConfig>(new AppConfig());
        }
    }

    return singleton;
}
};  // namespace graver