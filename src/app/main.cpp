#include <iostream>
#include <memory>

#include "graver/compiler/code_gen.h"
#include "graver/compiler/lexer.h"
#include "graver/compiler/parser.h"
#include "graver/compiler/print_visitor.h"
#include "graver/compiler/token.h"
#include "graver/util/log_util.h"

static void test_lexer() {
    std::shared_ptr<spdlog::logger> log    = graver::LogUtil::getLogger("app");
    const char*                     source = "5 + 1 - 3 * 4 / 2";
    graver::Lexer                   lexer(source);
    do {
        lexer.nextToken();
        auto token = lexer.currToken();
        SPDLOG_LOGGER_INFO(log, fmt::format("{}", graver::Token::toString(*token)));
    } while (lexer.currToken()->kind() != graver::TokenKind::Eof);
}

static void test_parser() {
    std::shared_ptr<spdlog::logger> log    = graver::LogUtil::getLogger("app");
    const char*                     source = "5 + 1 - 3 * 4 / 2";
    std::shared_ptr<graver::Lexer>  lexer  = std::make_shared<graver::Lexer>(source);
    lexer->nextToken();
    std::shared_ptr<graver::Parser> parser = std::make_shared<graver::Parser>(lexer);
    graver::PrintVisitor            visitor;
    auto                            root = parser->parse();
    root->accept(&visitor);
}

static void test_code_gen() {
    std::shared_ptr<spdlog::logger> log    = graver::LogUtil::getLogger("app");
    const char*                     source = "1+2+3+4*4*89-4*5*6/3";
    std::shared_ptr<graver::Lexer>  lexer  = std::make_shared<graver::Lexer>(source);
    lexer->nextToken();
    std::shared_ptr<graver::Parser> parser = std::make_shared<graver::Parser>(lexer);
    graver::CodeGen                 codegen;
    auto                            root = parser->parse();
    root->accept(&codegen);
}

int main() {
    graver::LogUtil::init(graver::AppConfig::getInstance()->logLevel(), "../logs/app.log");
    std::shared_ptr<spdlog::logger> log = graver::LogUtil::getLogger("app");
    SPDLOG_LOGGER_INFO(log, fmt::format("hello graver"));
    SPDLOG_LOGGER_INFO(log, fmt::format("test_lexer"));
    test_lexer();
    SPDLOG_LOGGER_INFO(log, fmt::format("test_parser"));
    test_parser();
    SPDLOG_LOGGER_INFO(log, fmt::format("test_code_gen"));
    test_code_gen();
    return 0;
}
