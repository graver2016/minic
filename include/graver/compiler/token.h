#pragma once
#include <memory>
#include <string>
namespace graver {
enum TokenKind {
    Eof,
    BAD,
    ADD,
    SUB,
    MUL,
    DIV,
    NUM
};

class Token {
public:
    Token() = default;
    Token(TokenKind kind, std::string text, int value);
    Token(TokenKind kind, std::string text);
    ~Token() = default;
    [[nodiscard]] TokenKind          kind() const;
    void                             kind(TokenKind kind);
    [[nodiscard]] int                value() const;
    void                             value(int value);
    [[nodiscard]] std::string        text() const;
    void                             text(std::string text);
    [[nodiscard]] static std::string toString(const Token& token);

private:
    TokenKind   m_kind{TokenKind::BAD};
    int         m_value{0};
    std::string m_text;
};

};  // namespace graver