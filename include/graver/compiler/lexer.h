#pragma once
#include <memory>
#include <string_view>

#include "graver/compiler/token.h"
namespace graver {

class Lexer {
public:
    Lexer() = default;
    explicit Lexer(const char* source);
    ~Lexer() = default;

public:
    [[nodiscard]] std::shared_ptr<Token> currToken();
    void                                 nextToken();
    void                                 parseNumberToken();
    void                                 nextChar();
    char                                 peek(size_t cursor = 0);

private:
    std::string_view       m_source;
    std::shared_ptr<Token> m_curr_token;
    char                   m_curr_char{'\0'};
    size_t                 m_cursor{0};
};

};  // namespace graver