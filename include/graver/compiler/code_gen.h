#pragma once

#include "graver/compiler/ast_node.h"
namespace graver {
class CodeGen : public AstVisitor {
public:
    CodeGen() = default;
    void visitorProgramNode(AstProgramNode* node) override;

private:
    void visitorBinaryNode(AstBinaryNode* node) override;
    void visitorConstantNode(AstConstantNode* node) override;
    void push();
    void pop(const char* reg);

private:
    int m_stack_level{0};
};

};  // namespace graver