#pragma once
#include <memory>

namespace graver {
class AstVisitor;
class AstNode {
public:
    virtual ~AstNode()                       = default;
    virtual void accept(AstVisitor* visitor) = 0;
};

class AstProgramNode : public AstNode {
public:
    AstProgramNode()           = default;
    ~AstProgramNode() override = default;
    void                                   accept(AstVisitor* visitor) override;
    [[nodiscard]] std::shared_ptr<AstNode> node() const;
    void                                   node(std::shared_ptr<AstNode> node);

private:
    std::shared_ptr<AstNode> m_node;
};

enum class BinaryOperator {
    ADD,
    SUB,
    MUL,
    DIV
};

class AstBinaryNode : public AstNode {
public:
    AstBinaryNode()           = default;
    ~AstBinaryNode() override = default;
    void accept(AstVisitor* visitor) override;

    [[nodiscard]] BinaryOperator op() const;
    void                         op(BinaryOperator op);

    [[nodiscard]] std::shared_ptr<AstNode> left() const;
    void                                   left(std::shared_ptr<AstNode> node);
    [[nodiscard]] std::shared_ptr<AstNode> right() const;
    void                                   right(std::shared_ptr<AstNode> node);

private:
    BinaryOperator           m_op{BinaryOperator::ADD};
    std::shared_ptr<AstNode> m_left;
    std::shared_ptr<AstNode> m_right;
};

class AstConstantNode : public AstNode {
public:
    AstConstantNode()           = default;
    ~AstConstantNode() override = default;
    void              accept(AstVisitor* visitor) override;
    [[nodiscard]] int value() const;
    void              value(int value);

private:
    int m_value{0};
};

class AstVisitor {
public:
    virtual void visitorProgramNode(AstProgramNode* node)   = 0;
    virtual void visitorBinaryNode(AstBinaryNode* node)     = 0;
    virtual void visitorConstantNode(AstConstantNode* node) = 0;
};

};  // namespace graver