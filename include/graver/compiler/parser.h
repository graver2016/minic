#pragma once

#include <memory>

#include "graver/compiler/ast_node.h"
#include "graver/compiler/lexer.h"
namespace graver {
class Parser {
public:
    explicit Parser(std::shared_ptr<Lexer> lexer);

public:
    std::shared_ptr<AstProgramNode> parse();
    std::shared_ptr<AstNode>        parseExpr();
    std::shared_ptr<AstNode>        parseAddExpr();
    std::shared_ptr<AstNode>        parseMultiExpr();
    std::shared_ptr<AstNode>        parsePrimaryExpr();

private:
    std::shared_ptr<Lexer> m_lexer;
};
};  // namespace graver