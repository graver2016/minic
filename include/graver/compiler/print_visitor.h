#pragma once

#include "graver/compiler/ast_node.h"
namespace graver {
class PrintVisitor : public AstVisitor {
    void visitorProgramNode(AstProgramNode* node) override;

private:
    void visitorBinaryNode(AstBinaryNode* node) override;
    void visitorConstantNode(AstConstantNode* node) override;
};
};  // namespace graver