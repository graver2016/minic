#pragma once
#include <spdlog/spdlog.h>

#include <memory>

namespace graver {

class AppConfig {
public:
    static std::shared_ptr<AppConfig> getInstance();

private:
    AppConfig();

public:
    spdlog::level::level_enum logLevel() {
        return this->m_log_level;
    }

private:
    /**
     * @brief 日志级别
     */
    spdlog::level::level_enum m_log_level;
};
};  // namespace graver
